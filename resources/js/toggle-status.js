$(document).ready(function(){
    $('.tggl-btn').change(function(){
        var userId = $(this).data('user-id');
        var status = $(this).prop('checked') ? 'Aktif' : 'Tidak Aktif';

        $.ajax({
            url: '/update-status',
            method: 'POST',
            data: { userId: userId, status: status },
            success: function(response) {
                // Handle response if needed
                console.log('Status updated successfully');
            },
            error: function(xhr, status, error) {
                console.error(error);
            }
        });
    });
});