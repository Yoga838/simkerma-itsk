@extends('base')
@section('title', 'SIMKERMA | Edit Profil')
@section('konten')
<div class="col-md-8">
    <h6 class="page-title">Edit Profil</h6>
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item"><a href="#">SIMKERMA</a></li>
        <li class="breadcrumb-item"><a href="{{ route('profil.show') }}">Profil</a></li>
        <li class="breadcrumb-item"><a href="#">Edit Profil</a></li>
    </ol>
</div>
<div class="row pt-3">
    <div class="col-md-4">
        <div class="card card-profile text-center">
            <div class="card-body d-flex flex-column align-items-center justify-content-center">
                <div class="image-upload">
                    <label for="file-input">
                        <img src="{{ $fotoProfil }}" alt="avatar">
                        <span class="pen-icon">
                            <img src="assets/images/icon-pen.png" alt="pen">
                        </span>
                    </label>
                    <form id="photo-form" action="{{ route('profil.update.foto', $user->id) }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <input id="file-input" type="file" name="foto" accept="image/*" style="display:none;">
                    </form>
                </div>
                <h5 class="nama pt-3">{{ $user->nama }}</h5>
                <p class="email">{{ $user->email }}</p>

            </div>
        </div>
    </div>

    <div class="col-md-8">
        <div class="card card-profile">
            <div class="card-body">
                <form action="{{ route('profil.update', $user->id) }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="row mb-3">
                        <label for="nama" class="col-sm-3 col-form-label">Nama Lengkap</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="text" value="{{ $user->nama }}" id="nama" name="nama">
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label for="email" class="col-sm-3 col-form-label">Alamat Email</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="email" value="{{ $user->email }}" id="email" autocomplete="off" name="email">
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label for="prodi" class="col-sm-3 col-form-label">Program Studi</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="text" value="{{ $user->prodi }}" id="prodi" name="prodi">
                        </div>
                    </div>
                    <div class="button-form d-flex justify-content-end align-items-end gap-2 pt-5">
                        <a href="{{ route('profil.show') }}" class="btn btn-danger waves-effect">Batal</a>
                        <button type="submit" class="btn btn-primary waves-effect waves-light"><i class="fas fa-save"></i> Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    document.getElementById('file-input').addEventListener('change', function() {
        document.getElementById('photo-form').submit();
    });
</script>
@endsection