<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        User::create([
            'nama' => 'Super Admin',
            'email' => 'email@gmail.com',
            'password' => bcrypt('123'), // Password Untuk admin: 123,
            'prodi' => '',
            'role' => 'Super Admin',
            'foto' => null,
            'is_active' => true,
        ]);
    }
}
