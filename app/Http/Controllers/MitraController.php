<?php

namespace App\Http\Controllers;

use App\Models\Mitra;
use App\Models\JenisMitra;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Rap2hpoutre\FastExcel\FastExcel;

class MitraController extends Controller
{
    public function index()
    {
        $mitra = Mitra::all();
        $jenisMitra = JenisMitra::all();
        return view('mitra.mitra', compact('mitra', 'jenisMitra'));
    }

    public function exportMitra()
    {
        $mitra = Mitra::all();
        $no = 1;

        (new FastExcel($mitra))->export('data-mitra.xlsx', function ($mitra) use (&$no) {
            return [
                'No' => $no++,
                'Nama Mitra' => $mitra->nama_mitra,
                'Jenis Mitra' => $mitra->jenis_mitra,
                'Nama PJ' => $mitra->penanggung_jawab,
                'Email' => $mitra->email,
                'No. HP' => $mitra->no_telp,
            ];
        });

        return response()->download('data-mitra.xlsx')->deleteFileAfterSend();
    }

    public function show(Mitra $mitra)
    {
        return view('mitra.mitra-show', compact('mitra'));
    }

    public function get_data(){
        $data = Mitra::all();
        return response()->json($data);
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'nama_mitra' => 'required|max:255',
            'jenis_mitra' => 'required|max:255',
            'penanggung_jawab' => 'required|max:255',
            'email' => 'required|email|unique:mitra',
            'no_telp' => 'required|max:15',
        ]);

        Mitra::create($validatedData);

        return redirect()->route('mitra.index')->with('success', 'Mitra berhasil ditambah!');
    }

    public function edit(Mitra $mitra)
    {
        return view('mitra.mitra-edit', compact('mitra'));
    }

    public function update(Request $request, Mitra $mitra)
    {
        $validatedData = $request->validate([
            'nama_mitra' => 'required|max:255',
            'jenis_mitra' => 'required|max:255',
            'penanggung_jawab' => 'required|max:255',
            'email' => 'required|email|unique:mitra,email,' . $mitra->id,
            'no_telp' => 'required|max:15',
        ]);

        $mitra->update($validatedData);

        return redirect()->route('mitra.index')->with('success', 'Mitra berhasil diubah!');
    }

    public function destroy(Request $request, $id)
    {
        $mitra = Mitra::find($id);

        if (!$mitra) {
            return redirect()->route('mitra.index')->with('error', 'Mitra tidak ditemukan!');
        }

        // Periksa apakah mitra memiliki data kerjasama
        $hasDataKerjasama = DB::table('data_kerjasama')->where('mitra_id', $mitra->id)->exists();

        if ($hasDataKerjasama) {
            if ($request->ajax()) {
                return response()->json(['error' => 'Mitra memiliki kerjasama dan tidak bisa dihapus.']);
            } else {
                return redirect()->route('mitra.index')->with('error', 'Mitra memiliki kerjasama dan tidak bisa dihapus!');
            }
        }

        $mitra->delete();

        if ($request->ajax()) {
            return response()->json(['success' => 'Mitra berhasil dihapus!']);
        } else {
            return redirect()->route('mitra.index')->with('success', 'Mitra berhasil dihapus!');
        }
    }
}
