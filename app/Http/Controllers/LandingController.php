<?php

namespace App\Http\Controllers;

use App\Models\Mitra;
use App\Models\Template;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class LandingController extends Controller
{
    public function showLanding(){
        $templates = Template::where('is_active', true)->get();
        return view('landingpage.index', compact('templates'));
    }
    public function downloadTemplates(Template $template)
    {
        $extension = pathinfo(Storage::url($template->file), PATHINFO_EXTENSION);
        return Storage::download($template->file, $template->nama . '.' . $extension);
    }
    public function showForm(){
        $mitra = Mitra::all();
        return view('landingpage.form', compact('mitra'));
    }
}
